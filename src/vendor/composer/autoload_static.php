<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitddfd7f83e67762e3dab35cc521182043
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'Predis\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Predis\\' => 
        array (
            0 => __DIR__ . '/..' . '/predis/predis/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitddfd7f83e67762e3dab35cc521182043::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitddfd7f83e67762e3dab35cc521182043::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitddfd7f83e67762e3dab35cc521182043::$classMap;

        }, null, ClassLoader::class);
    }
}
