<?php


namespace Lib;


class RedisClient
{
    //Redis 链接
    protected static $redisConnect = null;

    public function __construct()
    {
        if (!self::$redisConnect) {
            self::$redisConnect = (new \Redis());
        }
    }

    /**
     * 获取redis缓存
     * @param $key
     * @return bool|mixed|string
     */
    public static function get($key)
    {
        return self::$redisConnect->get($key);
    }

    /**
     * 设置redis缓存
     * @param $key
     * @param $value
     * @param int $expire
     */
    public static function set($key, $value, $expire = 60)
    {
        self::$redisConnect->set($key, $value);
        self::expire($key, $expire);
    }

    /**
     * 设置过期时间
     * @param $key
     * @param int $expire
     */
    private static function expire($key, $expire = 60)
    {
        self::$redisConnect->expire($key, $expire);
    }

    /**
     * 设置 锁
     * @param $key
     * @param $value
     * @param $expire
     * @return bool
     */
    public static function setNx($key, $value, $expire)
    {
        return self::$redisConnect->setnx($key, $value);
    }
}